// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_PlayerCameraBase_generated_h
#error "PlayerCameraBase.generated.h already included, missing '#pragma once' in PlayerCameraBase.h"
#endif
#define SNAKE_PlayerCameraBase_generated_h

#define Snake_Source_Snake_PlayerCameraBase_h_15_SPARSE_DATA
#define Snake_Source_Snake_PlayerCameraBase_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define Snake_Source_Snake_PlayerCameraBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define Snake_Source_Snake_PlayerCameraBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerCameraBase(); \
	friend struct Z_Construct_UClass_APlayerCameraBase_Statics; \
public: \
	DECLARE_CLASS(APlayerCameraBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(APlayerCameraBase)


#define Snake_Source_Snake_PlayerCameraBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerCameraBase(); \
	friend struct Z_Construct_UClass_APlayerCameraBase_Statics; \
public: \
	DECLARE_CLASS(APlayerCameraBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(APlayerCameraBase)


#define Snake_Source_Snake_PlayerCameraBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerCameraBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerCameraBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCameraBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCameraBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCameraBase(APlayerCameraBase&&); \
	NO_API APlayerCameraBase(const APlayerCameraBase&); \
public:


#define Snake_Source_Snake_PlayerCameraBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCameraBase(APlayerCameraBase&&); \
	NO_API APlayerCameraBase(const APlayerCameraBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCameraBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCameraBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerCameraBase)


#define Snake_Source_Snake_PlayerCameraBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_Snake_PlayerCameraBase_h_12_PROLOG
#define Snake_Source_Snake_PlayerCameraBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_PlayerCameraBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_PlayerCameraBase_h_15_SPARSE_DATA \
	Snake_Source_Snake_PlayerCameraBase_h_15_RPC_WRAPPERS \
	Snake_Source_Snake_PlayerCameraBase_h_15_INCLASS \
	Snake_Source_Snake_PlayerCameraBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_PlayerCameraBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_PlayerCameraBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_PlayerCameraBase_h_15_SPARSE_DATA \
	Snake_Source_Snake_PlayerCameraBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_PlayerCameraBase_h_15_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_PlayerCameraBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class APlayerCameraBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_PlayerCameraBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
